import multer from "multer";
import { Image } from "../models";


const upload = multer({ storage: multer.memoryStorage() });

export const getImages = (request, response) => {
    console.log("getImages");
    Image.find().then(uris => {
        response.send({imageURIs: uris, statusCode: 200 });
    });
};

export const postImage = [
    upload.single('picture'),
    (request, response) => {
        console.log("postImage");
        console.log(request.file);
       Image.create(request.file.buffer).then(uris => {
          response.send({ imageURI: uris, statusCode: 200});
       });
}
];