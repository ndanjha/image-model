import fs from "fs";
//import {getImagesResponse, postImageResponse} from "../apiStubs";

//set up
fs.promises.mkdir(process.env.RAZZLE_PUBLIC_DIR + "/images").catch(err => {
    if (err.code !== "EEXIT"){
        throw err;
    }
}); 

//fs reddir
const find = () => {
    return fs.promises.readdir(process.env.RAZZLE_PUBLIC_DIR + "/images")
    .then(fileNames => {
        return fileNames.map(fileName => "/images/" + fileName);
    });
    // return new Promise((resolve, reject) => {
    //   resolve(getImagesResponse.imageURIs);  
    // }) 
};
//fs  write file
 const create = buffer => {
    const timestamp = Date.now();
    return fs.promises.writeFile(
        process.env.RAZZLE_PUBLIC_DIR + "/images/" + timestamp,
        buffer
    ).then(() => {
        return "/images/" + timestamp;
    })
    // return new Promise((resolve, reject) =>{
    //     resolve(postImageResponse.imageURI)
    //})
};
    
export default {
    find,
    create
}